package institute;

import java.util.List;

/**
 *
 * @author mitpa
 */
public class GFG 
{
    public static void main(String[]args)
    {
       Book b1 = new Book("mit","hello");
       Book b2 = new Book("hey","hello");
       Book b3 = new Book("hi","hello");
       
       List<Book> books = new ArrayList<Book>();
       books.add(b1);
       books.add(b2);
       books.add(b3);
       
       Library library = new Library(books);
       
       List<Book> bks = library.getTotalBooksInLibrary();
       for(Book bk : bks)
       {
           System.out.println("title : " +bk.title+ " and " +"author : " +bk.author);
       }
       
    }
    
}
