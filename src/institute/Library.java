package institute;

import java.util.List;

/**
 *
 * @author mitpa
 */
public class Library 
{
    private final List<Book> books;

    public Library(List<Book> books) {
        this.books = books;
    }
    
    public List<Book> getTotalBooksInLibrary(){
        return books;
    }
    
    
    
}
